package br.com.desafioconcrete.desafioconcrete.domain;

/**
 * Created by Mateus Pontes on 26/03/15.
 */
public class RequestData {
    private String url;
    private String method;
    private Object obj;

    public RequestData(String url, String method, Object obj){
        this.url = url;
        this.method = method;
        this.obj = obj;
    }

    public String getUrl(){return this.url;}
    public void setUrl(String url){this.url = url;}

    public String getMethod(){return this.method;}
    public void setMethod(String method){this.method = method;}

    public Object getObj(){return this.obj;}
    public void setObj(Object obj){this.obj = obj;}
}
