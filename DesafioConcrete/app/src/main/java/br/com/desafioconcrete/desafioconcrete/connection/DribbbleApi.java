package br.com.desafioconcrete.desafioconcrete.connection;

/**
 * Created by geoambiente on 26/03/15.
 */
public class DribbbleApi {
    private static final String pathUrl = "http://api.dribbble.com/shots/";

    public static String getPopularShotsUrl(int currentPage) {
        return pathUrl + "popular?page=" + currentPage;
    }
}
