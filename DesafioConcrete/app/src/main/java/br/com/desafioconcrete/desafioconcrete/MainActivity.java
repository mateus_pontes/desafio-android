package br.com.desafioconcrete.desafioconcrete;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import br.com.desafioconcrete.desafioconcrete.connection.DribbbleApi;
import br.com.desafioconcrete.desafioconcrete.connection.Transaction;
import br.com.desafioconcrete.desafioconcrete.domain.RequestData;
import br.com.desafioconcrete.desafioconcrete.domain.Shot;


public class MainActivity extends ActionBarActivity implements Transaction {

    private Shot shot;
    private ProgressBar progressBar;
    private int maxPage;
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentPage = 1;//primeiro load na api pagina default = 1
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void doBefore() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void doAfter(String answer) {
        try
        {


        }

        finally {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public RequestData getRequestData() {
        return new RequestData(DribbbleApi.getPopularShotsUrl(currentPage), "get", shot);
    }
}
