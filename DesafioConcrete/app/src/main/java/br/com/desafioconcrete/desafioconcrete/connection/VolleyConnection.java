package br.com.desafioconcrete.desafioconcrete.connection;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import br.com.desafioconcrete.desafioconcrete.application.CustomApplication;
import br.com.desafioconcrete.desafioconcrete.domain.RequestData;

/**
 * Created by Mateus POntes on 26/03/15.
 */
public class VolleyConnection {
    private  Transaction transaction;
    private RequestQueue requestQueue;

    public VolleyConnection(Context context, Transaction trans){
        this.transaction = trans;
        requestQueue = ((CustomApplication) ((Activity) context).getApplication()).getRequestQueue();
    }

    public void execute(){
        transaction.doBefore();
        callByStringRequest(transaction.getRequestData());
    }

    private void callByStringRequest(final RequestData requestData) {
        StringRequest request = new StringRequest(Request.Method.GET,
                requestData.getUrl(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        transaction.doAfter(response);
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        transaction.doAfter(null);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> paramns = new HashMap<>();
                paramns.put("method", requestData.getMethod());

                return paramns;
            }
        };
    }
}
