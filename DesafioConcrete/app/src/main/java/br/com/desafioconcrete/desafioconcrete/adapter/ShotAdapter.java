package br.com.desafioconcrete.desafioconcrete.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import br.com.desafioconcrete.desafioconcrete.R;
import br.com.desafioconcrete.desafioconcrete.application.CustomApplication;
import br.com.desafioconcrete.desafioconcrete.domain.Shot;

/**
 * Created by Mateus Pontes on 28/03/15.
 */
public class ShotAdapter extends BaseAdapter {
    private List<Shot> shots;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;

    public ShotAdapter(Context c, List<Shot> s){
        this.shots = s;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ((CustomApplication) ((Activity)c).getApplication()).getImageLoader();
    }

    @Override
    public int getCount() {
        return shots.size();
    }

    @Override
    public Object getItem(int position) {
        return shots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return shots.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;

        if(view == null)
        {
            view = inflater.inflate(R.layout.list_shots, null);
            holder = new ViewHolder();
            view.setTag(holder);

            holder.image = (ImageView) view.findViewById(R.id.imgShot);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }

        holder.image.setImageBitmap(null);
        ///imageLoader.displayImage(shots.get(position));

        return view;
    }

    private static class ViewHolder
    {
        ImageView image;
    }
}
