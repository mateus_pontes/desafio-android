package br.com.desafioconcrete.desafioconcrete.connection;

import br.com.desafioconcrete.desafioconcrete.domain.RequestData;

/**
 * Created by Mateus Pontes  on 26/03/15.
 */
public interface Transaction {
    public void doBefore();
    public void doAfter(String answer);
    public RequestData getRequestData();
}
